package br.com.itau.modelospring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ModelospringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ModelospringApplication.class, args);
	}

}
