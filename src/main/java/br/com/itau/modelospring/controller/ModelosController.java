package br.com.itau.modelospring.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.modelospring.models.Modelos;
import br.com.itau.modelospring.services.ModelosService;

@RestController // Anotação para receber entrada e saída de dados na rede
public class ModelosController {
	@Autowired
	private ModelosService modeloService;

	@GetMapping("/modelo")
	public ArrayList<Modelos> listarModelos() {
		return modeloService.obterModelos();
		// ArrayList<Modelos> modelos = new ArrayList<>();

//		Modelos modelo1 = new Modelos();
//		modelo1.setId(1);
//		modelo1.setTitulo("Anita Mangueirão e Ponei Tarado");
//		modelo1.setAno(1976);
//
//		Modelos modelo2 = new Modelos();
//		modelo2.setId(2);
//		modelo2.setAno(2000);
//		modelo2.setTitulo("Senhor dos Anais");
//
//		modelos.add(modelo1);
//		modelos.add(modelo2);
//		// Incluindo na lista
//
//		return modelos;

	}

	@GetMapping("/modelo/{id}")
	public Modelos buscarModelos(@PathVariable int id) {
		Modelos modelos = modeloService.obterModelo(id);

		// ArrayList<Modelos> modelos = modeloService.obterModelos();

//		Modelos modelo1 = new Modelos();
//		modelo1.setId(1);
//		modelo1.setTitulo("Anita Mangueirão e Ponei Tarado");
//		modelo1.setAno(1976);
//
//		Modelos modelo2 = new Modelos();
//		modelo2.setId(2);
//		modelo2.setAno(2000);
//		modelo2.setTitulo("Senhor dos Anas");
//
//		modelos.add(modelo1);
//		modelos.add(modelo2);

		Modelos modelo = modeloService.obterModelo(id);

		if (modelo == null)
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);

		return modelo;
		// return null;
	}

	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public void criarModelos(@RequestBody Modelos modelo) {
		modeloService.inserirModelos(modelo);
	}

}