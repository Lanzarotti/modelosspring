package br.com.itau.modelospring.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.modelospring.models.Modelos;

@RestController // Anotação para receber entrada e saída de dados na rede
public class HelloController {
	@GetMapping("/hello") // Método Get para buscar dados
	public Modelos dizerHello() {
		Modelos modelos = new Modelos();
		modelos.setTitulo("Mazzaroppi");
		modelos.setAno(1975);

		return modelos;
	}

	@GetMapping("/milf") // Método Get para buscar dados
	public String helloAgain() {
		return "MILF";
	}

}
