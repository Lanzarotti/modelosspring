package br.com.itau.modelospring.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import br.com.itau.modelospring.models.Modelos;

@Service
public class ModelosService {
	ArrayList<Modelos> modelos;

	public ModelosService() {
		modelos = new ArrayList<Modelos>();

		Modelos modelo1 = new Modelos();
		modelo1.setId(1);
		modelo1.setTitulo("Anita Mangueirão e Ponei Tarado");
		modelo1.setAno(1976);

		Modelos modelo2 = new Modelos();
		modelo2.setId(2);
		modelo2.setAno(2000);
		modelo2.setTitulo("Senhor dos Aneis");

		modelos.add(modelo1);
		modelos.add(modelo2);

	}

	public ArrayList<Modelos> obterModelos() {
		return modelos;
	}

	public Modelos obterModelo(int id) {
		for(Modelos modelo: modelos) {
			if (modelo.getId() == id) {
				return modelo;
												
			}
		}
		return null;
	}
	
	
	
	public void inserirModelos(Modelos modelo) {
		modelos.add(modelo);
	}
}
